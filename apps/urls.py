from django.urls import path
from . views import *

urlpatterns = [
    path('yt_summary/', predict_summary, name='predict_summary'),
]