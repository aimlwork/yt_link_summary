from rest_framework.decorators import api_view
from rest_framework.response import Response
from gradio_client import Client

@api_view(['POST'])
def predict_summary(request):
    if request.method == 'POST':
        youtube_url = request.data.get('url')
        if youtube_url:
            client = Client("cfc-tech/summary_tube")
            result = client.predict(url=youtube_url, api_name="/predict")
            return Response(result)
        else:
            return Response({'error': 'Invalid request. "url" parameter is missing.'}, status=400)
    else:
        return Response({'error': 'Invalid request method. Only POST is allowed.'}, status=405)
